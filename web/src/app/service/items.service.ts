import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ApiHttpClientService } from './api-http-client.service';

@Injectable({
  providedIn: 'root'
})
export class ItemsService extends ApiHttpClientService {

  constructor(private http: HttpClient) {
    super()
  }

  get() {
    return this.http.get(this.baseUrl.concat('items'))
  }
}
