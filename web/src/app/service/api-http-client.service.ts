import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ApiHttpClientService {

  public baseUrl: string

  constructor() {
    this.baseUrl = 'http://localhost:8080/'
  }
}
