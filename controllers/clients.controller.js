const { pool } = require("../db/db");

const getClients = async (req, res) => {
    const response = await pool.query('SELECT * FROM client ORDER BY id ASC');
    res.status(200).json(response.rows);
};

const getClientsById = async (req, res) => {
    const id = parseInt(req.params.id);
    const response = await pool.query('SELECT * FROM client WHERE id = $1', [id]);
    res.json(response.rows);
};

const createClients = async (req, res) => {
    const { names, lastnames } = req.body;
    await pool.query('INSERT INTO client(names, lastnames) VALUES ($1, $2)', [names, lastnames]);
    res.status(201).json()
};

const updateClients = async (req, res) => {
    const id = parseInt(req.params.id);
    const { names, lastnames } = req.body;

    const response = await pool.query('UPDATE client SET names= $1, lastnames= $2 WHERE id = $3', [
        names,
        lastnames,
        id
    ]);
    res.status(response.rowCount > 0 ? 200 : 404).json();
};

const deleteClients = async (req, res) => {
    const id = parseInt(req.params.id);

    await pool.query('DELETE FROM items ite USING orders ord  WHERE ord.id = ite.order_id AND ord.client_id = $1', [id])

    await pool.query('DELETE FROM orders WHERE client_id = $1', [id])

    const response = await pool.query('DELETE FROM client WHERE id = $1', [id]);

    res.status(response.rowCount > 0 ? 200 : 404).json();
};

module.exports = {
    getClients,
    getClientsById,
    createClients,
    updateClients,
    deleteClients
};

