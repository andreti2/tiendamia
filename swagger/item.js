/**
 * @swagger
 * components:
 *   schemas:
 *     Item:
 *       type: object
 *       required:
 *         - title
 *         - description
 *         - url
 *         - price
 *         - quantity 
 *         - order_id 
 *       properties:
 *         id:
 *           type: string
 *           description: Valor autogenerado para el id del item
 *         title:
 *           type: string
 *           description: Titulo del item
 *         description:
 *           type: string
 *           description: Description del item
 *         url:
 *           type: string
 *           description: Url del item
 *         price:
 *           type: decimal
 *           description: Precio del item
 *         quantity:
 *           type: number
 *           description: Cantidad del item
 *         order_id:
 *           type: number
 *           description: Id de la orden a la que pertenece
 *       example:
 *         title: Item 1
 *         description: Descripcion del item
 *         url: www.myitem.com
 *         price: 10.99
 *         quantity: 10
 *         order_id: 1
 */

/**
 * @swagger
 * tags:
 *   name: Items
 *   description: Items
 * /items:
 *   get:
 *     summary: Trae la lista de items
 *     tags: [Items]
 *     responses:
 *       200:
 *         description: Retorna la lista de items
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/Item'
 *       500:
 *         description: Error en el servidor
 * 
 *   post:
 *     summary: Crea un item
 *     tags: [Items]
 *     requestBody:
 *        required: true
 *        content:
 *            application/json:
 *                schema:
 *                    $ref: '#/components/schemas/Item'
 *     responses:
 *       201:
 *         description: El item fue creado.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/Item'
 *       500:
 *         description: Error en el servidor
 *
 * /items/{id}:
 *   get:
 *     summary: Trae el item por id
 *     tags: [Items]
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *         required: true
 *         description: Id del item
 *     responses:
 *       200:
 *         description: Retorna el item encontrado o vacio en caso contrario
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/Item'
 *       500:
 *         description: Error en el servidor
 *   put:
 *     summary: Actualiza el item con el id dado
 *     tags: [Items]
 *     requestBody:
 *        required: true
 *        content:
 *            application/json:
 *                schema:
 *                    $ref: '#/components/schemas/Item'
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *         required: true
 *         description: Id del item
 *     responses:
 *       200:
 *         description: Retorna el item encontrado o vacio en caso contrario
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/Item'
 *       404:
 *         description: No se encontro el item
 *       500:
 *         description: Error en el servidor
 *   delete:
 *     summary: Elimina un item por id
 *     tags: [Items]
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *         required: true
 *         description: Id del item
 *
 *     responses:
 *       200:
 *         description: El item se elimino correctamente
 *       404:
 *         description: No se encontro el item
 * 
 * /items/order/{order_id}:
 *   get:
 *     summary: Trae un listado de items por el id de la orden
 *     tags: [Items]
 *     parameters:
 *       - in: path
 *         name: order_id
 *         schema:
 *           type: string
 *         required: true
 *         description: Id de la orden
 *     responses:
 *       200:
 *         description: Retorna la lista de items encontrados o vacio en caso contrario
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/Item'
 *       500:
 *         description: Error en el servidor
 */