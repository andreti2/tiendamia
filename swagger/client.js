/**
 * @swagger
 * components:
 *   schemas:
 *     Client:
 *       type: object
 *       required:
 *         - names
 *         - lastnames
 * 
 *       properties:
 *         id:
 *           type: string
 *           description: Valor autogenerado para el id de Client
 *         names:
 *           type: string
 *           description: Id del Cliente
 *         lastnames:
 *           type: string
 *           description: Direccion de envio
 * 
 *       example:
 *         names: Pedro Juan
 *         lastnames: Perez Romo
 */

/**
 * @swagger
 * tags:
 *   name: Clients
 *   description: Clients
 * /clients:
 *   get:
 *     summary: Trae la lista de Clients
 *     tags: [Clients]
 *     responses:
 *       200:
 *         description: Retorna la lista de Clients
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/Client'
 *       500:
 *         description: Error en el servidor
 * 
 *   post:
 *     summary: Crea una Orden
 *     tags: [Clients]
 *     requestBody:
 *        required: true
 *        content:
 *            application/json:
 *                schema:
 *                    $ref: '#/components/schemas/Client'
 *     responses:
 *       201:
 *         description: El Client fue creado.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/Client'
 *       500:
 *         description: Error en el servidor
 * 
 * /clients/{id}:
 *   get:
 *     summary: Trae el Client por id
 *     tags: [Clients]
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *         required: true
 *         description: Id del Client
 *     responses:
 *       200:
 *         description: Retorna el Client encontrado o vacio en caso contrario
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/Client'
 *       500:
 *         description: Error en el servidor
 *   put:
 *     summary: Actualiza el Client con el id dado
 *     tags: [Clients]
 *     requestBody:
 *        required: true
 *        content:
 *            application/json:
 *                schema:
 *                    $ref: '#/components/schemas/Client'
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *         required: true
 *         description: Id del Client
 *     responses:
 *       200:
 *         description: Actualizo correctamente el registro
 *       404:
 *         description: No se encontro el Client
 *       500:
 *         description: Error en el servidor
 *   delete:
 *     summary: Elimina un Client por id
 *     tags: [Clients]
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *         required: true
 *         description: Id del Client
 *
 *     responses:
 *       200:
 *         description: El Client se elimino correctamente
 *       404:
 *         description: No se encontro el Client
 */