/**
 * @swagger
 * components:
 *   schemas:
 *     OrderItem:
 *       type: object
 *       required:
 *         - order_id
 *         - items_id
 *         - quantity
 * 
 *       properties:
 *         id:
 *           type: string
 *           description: Valor autogenerado para el id de Order
 *         order_id:
 *           type: number
 *           description: Id de la orden
 *         items_id:
 *           type: number
 *           description: Id del item
 *         quantity:
 *           type: date
 *           description: Fecha de promesa de envio
 *       example:
 *         order_id: 1
 *         items_id: 1
 *         quantity: 1
 */

/**
 * @swagger
 * tags:
 *   name: OrdersItems
 *   description: Orders items
 * /orders-items:
 *   get:
 *     summary: Trae la lista de Orders items
 *     tags: [OrdersItems]
 *     responses:
 *       200:
 *         description: Retorna la lista de Orders items
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/Order'
 *       500:
 *         description: Error en el servidor
 *   post:
 *     summary: Crea una Orden
 *     tags: [OrdersItems]
 *     requestBody:
 *        required: true
 *        content:
 *            application/json:
 *                schema:
 *                    $ref: '#/components/schemas/OrderItem'
 *     responses:
 *       201:
 *         description: El Order item fue creado.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/OrderItem'
 *       500:
 *         description: Error en el servidor
 * 
 * 
 * /orders-items/{id}:
 *   get:
 *     summary: Trae el Order item por id
 *     tags: [OrdersItems]
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *         required: true
 *         description: Id del Order item
 *     responses:
 *       200:
 *         description: Retorna el Order item encontrado o vacio en caso contrario
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/OrderItem'
 *       500:
 *         description: Error en el servidor
 *   delete:
 *     summary: Elimina un Order por id
 *     tags: [OrdersItems]
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *         required: true
 *         description: Id del Order
 *
 *     responses:
 *       200:
 *         description: El Order se elimino correctamente
 *       404:
 *         description: No se encontro el Order
 * 
 * /order-items/order/{order_id}:
 *   get:
 *     summary: Trae un listado de items por el id de la orden
 *     tags: [OrdersItems]
 *     parameters:
 *       - in: path
 *         name: order_id
 *         schema:
 *           type: string
 *         required: true
 *         description: Id de la orden
 *     responses:
 *       200:
 *         description: Retorna la lista de items encontrados o vacio en caso contrario
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/OrderItem'
 *       500:
 *         description: Error en el servidor
 * 
 *   delete:
 *     summary: Elimina los OrderItems por el id de orden
 *     tags: [OrdersItems]
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *         required: true
 *         description: Id del Order
 *
 *     responses:
 *       200:
 *         description: se elimino correctamente los order items
 */