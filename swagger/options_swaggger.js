
function getOptionsSwagger(port) {
    return {
        definition: {
            openapi: "3.1.0",
            info: {
                title: "Tiendamia Swagger",
                version: "0.1.0",
                description:
                    "Prueba tecnica NodeJs",
                license: {
                    name: "MIT",
                    url: "https://spdx.org/licenses/MIT.html",
                },
                contact: {
                    name: "Andres Santacruz",
                    email: "casz21@hotmail.com",
                },
            },
            servers: [
                {
                    url: `http://localhost:${port}`,
                },
            ],
        },
        apis: ["./swagger/*.js"],
    }
}


module.exports = {
    getOptionsSwagger
}