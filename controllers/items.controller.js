
const { pool } = require("../db/db");

const getItems = async (req, res) => {
    const response = await pool.query('SELECT * FROM items ORDER BY id ASC');
    res.status(200).json(response.rows);
};

const getItemsById = async (req, res) => {
    const id = parseInt(req.params.id);
    const response = await pool.query('SELECT * FROM items WHERE id = $1', [id]);
    res.json(response.rows);
};

const createItems = async (req, res) => {
    const { title, description, url, price, quantity } = req.body;
    await pool.query('INSERT INTO items(title, description, url, price, quantity) VALUES ($1, $2, $3, $4, $5)',
        [title, description, url, price, quantity]);
    res.status(201).json()
};

const updateItems = async (req, res) => {
    const id = parseInt(req.params.id);
    const { title, description, url, price, quantity } = req.body;

    const response = await pool.query('UPDATE items SET title = $1, description = $2, url = $3, price = $4, quantity= $5 WHERE id = $6', [
        title,
        description,
        url,
        price,
        quantity,
        id
    ]);
    res.status(response.rowCount > 0 ? 200 : 404).json();
};

const deleteItems = async (req, res) => {
    const id = parseInt(req.params.id);
    const response = await pool.query('DELETE FROM items where id = $1', [
        id
    ]);
    res.status(response.rowCount > 0 ? 200 : 404).json();
};

async function getStockItem(id, quantity) {
    const response = await pool.query('SELECT * FROM items WHERE id = $1 AND quantity >= $2', [id, quantity]);
    return response.rowCount > 0 ? response.rows[0].quantity : -1
};

async function updateStockItem(id, newStock){
    const response = await pool.query('UPDATE items SET quantity= (quantity - $1) WHERE id = $2', [
        newStock,
        id
    ]);

    return response.rowCount > 0
}

module.exports = {
    getItems,
    getItemsById,
    createItems,
    updateItems,
    deleteItems,
    getStockItem,
    updateStockItem
};

