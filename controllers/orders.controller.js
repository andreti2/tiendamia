const { pool } = require("../db/db");
const { getStockItem, updateStockItem } = require("./items.controller");
const { createOrderItemsFromList, deleteOrderItemsFromOrder } = require("./order_items.controller");


const getOrders = async (req, res) => {
    const response = await pool.query('SELECT * FROM orders ORDER BY id ASC');
    res.status(200).json(response.rows);
};

const getOrdersById = async (req, res) => {
    const id = parseInt(req.params.id);
    const response = await pool.query('SELECT * FROM orders WHERE id = $1', [id]);
    res.json(response.rows);
};

const getOrdersByClientId = async (req, res) => {
    const client_id = parseInt(req.params.client_id);
    const response = await pool.query('SELECT * FROM orders WHERE client_id = $1', [client_id]);
    res.json(response.rows);
};

const createOrders = async (req, res) => {
    const { client_id, shipping_address, shipping_promise, items } = req.body;

    const itemsStock = []
    for (const item of items) {
        const stock = await getStockItem(item.items_id, item.quantity)

        if (stock == -1) {
            res.status(400).json({ message: `El item con id=${item.items_id} no tiene stock disponible` })
            return
        }

        item['stock'] = stock

        itemsStock.push(item)
    }

    const { rows } = await pool.query('INSERT INTO orders(client_id, shipping_address, shipping_promise) VALUES ($1, $2, $3) RETURNING id',
        [client_id, shipping_address, shipping_promise]);

    const order_id = rows[0].id

    for (const item of itemsStock) {
        await updateStockItem(item.items_id, item.quantity)
    }

    await createOrderItemsFromList(itemsStock, order_id)

    res.status(201).json()
};

const updateOrders = async (req, res) => {
    const id = parseInt(req.params.id);
    const { status, client_id, shipping_address, shipping_promise } = req.body;

    const response = await pool.query('UPDATE orders SET status= $1, client_id= $2, shipping_address= $3, shipping_promise= $4 WHERE id = $5', [
        status,
        client_id,
        shipping_address,
        shipping_promise,
        id
    ]);
    res.status(response.rowCount > 0 ? 200 : 404).json();
};

const deleteOrders = async (req, res) => {
    const id = parseInt(req.params.id);

    deleteOrderItemsFromOrder(id)

    await pool.query('DELETE FROM orders WHERE id = $1', [
        id
    ]);
    res.json();
};


module.exports = {
    getOrders,
    getOrdersById,
    createOrders,
    updateOrders,
    deleteOrders,
    getOrdersByClientId
};

