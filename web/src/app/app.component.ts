import { Component } from '@angular/core';
import { ItemsService } from './service/items.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'client';

  constructor(private _itemsService: ItemsService) {
    this._itemsService.get().subscribe((res) => {
      console.log(res)
    })
  }


}
