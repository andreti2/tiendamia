SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;


-- TABLE ITEMS

CREATE TABLE public.items (
    id integer NOT NULL,
    create_date timestamp NOT NULL DEFAULT NOW(),
    title character varying(80) NOT NULL,
    description character varying(100) NOT NULL,
    url character varying(100) NOT NULL,
    price decimal NOT NULL,
    quantity integer NOT NULL
);

ALTER TABLE public.items OWNER TO postgres;

CREATE SEQUENCE public.items_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER TABLE public.items_id_seq OWNER TO postgres;

ALTER SEQUENCE public.items_id_seq OWNED BY public.items.id;

ALTER TABLE ONLY public.items ALTER COLUMN id SET DEFAULT nextval('public.items_id_seq'::regclass);


-- TABLE ORDERS

CREATE TABLE public.orders (
    id integer NOT NULL,
    create_date timestamp NOT NULL DEFAULT NOW(),
    status character varying(100) NOT NULL DEFAULT 'Approve',
    shipping_address character varying(100) NOT NULL,
    shipping_promise timestamp NOT NULL,
    client_id integer NOT NULL
);

ALTER TABLE public.orders OWNER TO postgres;

CREATE SEQUENCE public.orders_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER TABLE public.orders_id_seq OWNER TO postgres;

ALTER SEQUENCE public.orders_id_seq OWNED BY public.orders.id;

ALTER TABLE ONLY public.orders ALTER COLUMN id SET DEFAULT nextval('public.orders_id_seq'::regclass);


-- TABLE ORDERS_ITEMS

CREATE TABLE public.orders_items (
    id integer NOT NULL,
    create_date timestamp NOT NULL DEFAULT NOW(),
    order_id integer NOT NULL,
    items_id integer NOT NULL,
    quantity integer NOT NULL
);

ALTER TABLE public.orders_items OWNER TO postgres;

CREATE SEQUENCE public.orders_items_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER TABLE public.orders_items_id_seq OWNER TO postgres;

ALTER SEQUENCE public.orders_items_id_seq OWNED BY public.orders_items.id;

ALTER TABLE ONLY public.orders_items ALTER COLUMN id SET DEFAULT nextval('public.orders_items_id_seq'::regclass);

-- TABLE CLIENT
CREATE TABLE public.client (
    id integer NOT NULL,
    create_date timestamp NOT NULL DEFAULT NOW(),
    names character varying(100) NOT NULL,
    lastnames character varying(100) NOT NULL
);


ALTER TABLE public.client OWNER TO postgres;

CREATE SEQUENCE public.client_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER TABLE public.client_id_seq OWNER TO postgres;

ALTER SEQUENCE public.client_id_seq OWNED BY public.client.id;

ALTER TABLE ONLY public.client ALTER COLUMN id SET DEFAULT nextval('public.client_id_seq'::regclass);


-- PRIMARY KEY

ALTER TABLE ONLY public.items
    ADD CONSTRAINT items_pkey PRIMARY KEY (id);

ALTER TABLE ONLY public.orders
    ADD CONSTRAINT orders_pkey PRIMARY KEY (id);


ALTER TABLE ONLY public.client
    ADD CONSTRAINT client_pkey PRIMARY KEY (id);

ALTER TABLE ONLY public.orders_items
    ADD CONSTRAINT orders_items_pkey PRIMARY KEY (id);

-- FOREIGN KEY

ALTER TABLE ONLY public.orders
    ADD CONSTRAINT orders_client_fkey FOREIGN KEY (client_id) REFERENCES public.client(id);

ALTER TABLE ONLY public.orders_items
    ADD CONSTRAINT orders_items_items_fkey FOREIGN KEY (items_id) REFERENCES public.items(id);

ALTER TABLE ONLY public.orders_items
    ADD CONSTRAINT orders_items_orders_fkey FOREIGN KEY (order_id) REFERENCES public.orders(id);


-- DATA

COPY public.client (id, names, lastnames) FROM stdin;
1	Pedro	Perez
\.

COPY public.orders (id, status, client_id, shipping_address, shipping_promise) FROM stdin;
1	Approve	1	carrera falsa 123	2023-08-08 00:00:00.000
\.

COPY public.items (id, title, description, url, price, quantity) FROM stdin;
1	disk drive	External DVD Drive USB 3.0	https://www.amazon.com/	28	10
2	Memory ram DDR5	KingSpec DDR5 Ram 32GB	https://www.amazon.com/	120	20
3	Memory ram DDR4	KingSpec DD45 Ram 16GB	https://www.amazon.com/	69	50
4	Mouse	Rizer 360G 40000PI	www.amazon.com	90	50
\.

COPY public.orders_items (id, order_id, items_id, quantity) FROM stdin;
1	1	1	1
\.

-- SEQ

SELECT pg_catalog.setval('public.items_id_seq', 4, true);

SELECT pg_catalog.setval('public.orders_id_seq', 1, true);

SELECT pg_catalog.setval('public.client_id_seq', 1, true);

SELECT pg_catalog.setval('public.orders_items_id_seq', 1, true);