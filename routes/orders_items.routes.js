const { Router } = require('express');
const router = Router();

const { getOrderItems, getOrderItemsById, getOrderItemsByOrderId, createOrderItemsFromList, deleteOrderItems, deleteOrderItemsFromOrder } = require('../controllers/order_items.controller');

router.get('/orders-items', getOrderItems);
router.post('/orders-items', createOrderItemsFromList);
router.get('/orders-items/:id', getOrderItemsById);
router.delete('/orders-items/:id', deleteOrderItems)
router.get('/orders-items/order/:order_id', getOrderItemsByOrderId);
router.delete('/orders-items/order/:order_id', deleteOrderItemsFromOrder)

module.exports = router;