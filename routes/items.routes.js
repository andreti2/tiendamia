const { Router } = require('express');
const router = Router();

const { getItems, getItemsById, updateItems, deleteItems, createItems } = require('../controllers/items.controller');

router.get('/items', getItems);
router.get('/items/:id', getItemsById);
router.post('/items', createItems);
router.put('/items/:id', updateItems)
router.delete('/items/:id', deleteItems);

module.exports = router;