require('dotenv').config();
const express = require("express"),
    bodyParser = require("body-parser"),
    swaggerJsdoc = require("swagger-jsdoc"),
    swaggerUi = require("swagger-ui-express");
const { getOptionsSwagger } = require("./swagger/options_swaggger");
const cors = require('cors');
const app = express();
const port = process.env.PORT || 3000

// middlewares
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cors({
    origin: '*'
}))

// swagger
const specs = swaggerJsdoc(getOptionsSwagger(8080));
app.use(
    "/api-docs",
    swaggerUi.serve,
    swaggerUi.setup(specs, { explorer: true })
);

// routes
app.use(require('./routes/clients.routes'));
app.use(require('./routes/items.routes'));
app.use(require('./routes/orders.routes'))
app.use(require('./routes/orders_items.routes'))
app.get('/', (req, res) => {
    res.json({
        "message": "server running"
    })
})


app.listen(port);
console.log('Server on port', port);