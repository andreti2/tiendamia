
const { pool } = require("../db/db");

const getOrderItems = async (req, res) => {
    const response = await pool.query('SELECT * FROM orders_items ORDER BY id ASC');
    res.status(200).json(response.rows);
};

const getOrderItemsById = async (req, res) => {
    const id = parseInt(req.params.id);
    const response = await pool.query('SELECT * FROM orders_items WHERE id = $1', [id]);
    res.json(response.rows);
};

const getOrderItemsByOrderId = async (req, res) => {
    const order_id = parseInt(req.params.order_id);
    const response = await pool.query('SELECT * FROM orders_items WHERE order_id = $1', [order_id]);
    res.json(response.rows);
};


const deleteOrderItems = async (req, res) => {
    const id = parseInt(req.params.id);
    const response = await pool.query('DELETE FROM orders_items WHERE id = $1', [
        id
    ]);
    res.status(response.rowCount > 0 ? 200 : 404).json();
};

function createOrderItemsFromList(items, order_id) {
    items.forEach(async item => {
        const { items_id, quantity } = item;
        await pool.query('INSERT INTO orders_items(items_id, quantity, order_id) VALUES ($1, $2, $3)',
            [items_id, quantity, order_id]);
    });
}

async function deleteOrderItemsFromOrder(order_id) {
    await pool.query('DELETE FROM orders_items WHERE order_id = $1', [
        order_id
    ]);
}

module.exports = {
    getOrderItems,
    getOrderItemsById,
    getOrderItemsByOrderId,
    deleteOrderItems,
    deleteOrderItemsFromOrder,
    createOrderItemsFromList,
};

