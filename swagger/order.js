/**
 * @swagger
 * components:
 *   schemas:
 *     Order:
 *       type: object
 *       required:
 *         - client_id
 *         - shipping_address
 *         - shipping_promise
 * 
 *       properties:
 *         id:
 *           type: string
 *           description: Valor autogenerado para el id de Order
 *         status:
 *           type: string
 *           description: Status de l Order
 *           enum:
 *             - Approve
 *             - Cancel
 *             - Delivery
 *             - Traveling
 *         client_id:
 *           type: number
 *           description: Id del Cliente
 *         shipping_address:
 *           type: date
 *           description: Direccion de envio
 *         shipping_promise:
 *           type: date
 *           description: Fecha de promesa de envio
 *         items:
 *           type: array
 *           description: Items de la orden
 *           items:
 *             $ref: '#/components/schemas/OrderItem'
 *       example:
 *         client_id: 1
 *         shipping_address: 10.99
 *         shipping_promise: 2020-03-10T04:05:06.157Z
 *         items: [{ order_id: 1,items_id: 1 , quantity: 10}]
 *     OrderPut:
 *       type: object
 *       required:
 *         - client_id
 *         - shipping_address
 *         - shipping_promise
 * 
 *       properties:
 *         id:
 *           type: string
 *           description: Valor autogenerado para el id de Order
 *         status:
 *           type: string
 *           description: Status de l Order
 *           enum:
 *             - Approve
 *             - Cancel
 *             - Delivery
 *             - Traveling
 *         client_id:
 *           type: number
 *           description: Id del Cliente
 *         shipping_address:
 *           type: date
 *           description: Direccion de envio
 *         shipping_promise:
 *           type: date
 *           description: Fecha de promesa de envio
 *       example:
 *         client_id: 1
 *         shipping_address: 10.99
 *         shipping_promise: 2020-03-10T04:05:06.157Z
 */

/**
 * @swagger
 * tags:
 *   name: Orders
 *   description: Orders
 * /orders:
 *   get:
 *     summary: Trae la lista de Orders
 *     tags: [Orders]
 *     responses:
 *       200:
 *         description: Retorna la lista de Orders
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/Order'
 *       500:
 *         description: Error en el servidor
 * 
 *   post:
 *     summary: Crea una Orden
 *     tags: [Orders]
 *     requestBody:
 *        required: true
 *        content:
 *            application/json:
 *                schema:
 *                    $ref: '#/components/schemas/Order'
 *     responses:
 *       201:
 *         description: El Order fue creado.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/Order'
 *       500:
 *         description: Error en el servidor
 * 
 * /orders/{id}:
 *   get:
 *     summary: Trae el Order por id
 *     tags: [Orders]
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *         required: true
 *         description: Id del Order
 *     responses:
 *       200:
 *         description: Retorna el Order encontrado o vacio en caso contrario
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/Order'
 *       500:
 *         description: Error en el servidor
 *   put:
 *     summary: Actualiza el Order con el id dado
 *     tags: [Orders]
 *     requestBody:
 *        required: true
 *        content:
 *            application/json:
 *                schema:
 *                    $ref: '#/components/schemas/OrderPut'
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *         required: true
 *         description: Id del Order
 *     responses:
 *       200:
 *         description: Actualizo correctamente el registros
 *       404:
 *         description: No se encontro el Order
 *       500:
 *         description: Error en el servidor
 *   delete:
 *     summary: Elimina un Order por id
 *     tags: [Orders]
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *         required: true
 *         description: Id del Order
 *
 *     responses:
 *       200:
 *         description: El Order se elimino correctamente
 *       404:
 *         description: No se encontro el Order
 */