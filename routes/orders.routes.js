const { Router } = require('express');
const router = Router();

const { getOrders, getOrdersById, updateOrders, deleteOrders, createOrders, getOrdersByClientId } = require('../controllers/orders.controller');

router.get('/orders', getOrders);
router.get('/orders/:id', getOrdersById);
router.get('/orders/client/:client_id', getOrdersByClientId);
router.post('/orders', createOrders);
router.put('/orders/:id', updateOrders)
router.delete('/orders/:id', deleteOrders);

module.exports = router;